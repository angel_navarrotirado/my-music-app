import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import {
  Item,
  ResponseSpotify,
} from 'src/app/shared/interfaces/spotify.interface';
import { Track } from 'src/app/shared/interfaces/track.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SpotifyService {
  private API_URL = environment.API;

  constructor(private http: HttpClient) {}

  getListTracks() {
    return this.http
      .get<ResponseSpotify>(this.API_URL + 'me/player/recently-played')
      .pipe(map(({ items }) => items.map(({ track }) => track)));
  }

  getListFavoritos() {
    let arrFavoritos: any = localStorage.getItem('favoritos');
    if (arrFavoritos) {
      arrFavoritos = JSON.parse(arrFavoritos);
    } else {
      arrFavoritos = [];
    }
    return arrFavoritos;
  }

  guardarFavoritos(track: Track) {
    let arrFavoritos = this.getListFavoritos();
    arrFavoritos.push(track);
    localStorage.setItem('favoritos', JSON.stringify(arrFavoritos));
  }

  eliminarFavoritos(id: string) {
    let arrFavoritos = this.getListFavoritos();
    arrFavoritos = arrFavoritos.filter((track: Track) => track.id !== id);
    localStorage.setItem('favoritos', JSON.stringify(arrFavoritos));
  }
}
