import { Component, OnInit } from '@angular/core';
import { Item } from 'src/app/shared/interfaces/spotify.interface';
import { Track } from 'src/app/shared/interfaces/track.interface';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-list-tracks',
  templateUrl: './list-tracks.component.html',
  styleUrls: ['./list-tracks.component.scss'],
})
export class ListTracksComponent implements OnInit {
  public listTracks: Track[] = [];
  public mostrarLoading = false;

  constructor(private spotifyService: SpotifyService) {}

  ngOnInit(): void {
    this.cargarListaDeCanciones();
  }

  cargarListaDeCanciones() {
    this.mostrarLoading = true;
    this.spotifyService.getListTracks().subscribe((response) => {
      /* Logica para pintar favoritos en el home */
      const arrFavoritos = this.spotifyService.getListFavoritos();
      this.listTracks = response.map((track) => {
        for (let i = 0; i < arrFavoritos.length; i++) {
          if (track.id === arrFavoritos[i].id) {
            track.clase = 'fa-solid';
          }
        }
        return track;
      });
      this.mostrarLoading = false;
    });
  }

  addFavoritos(track: Track) {
    if (track.clase === 'fa-solid') {
      track.clase = 'fa-regular';
      this.spotifyService.eliminarFavoritos(track.id);
    } else {
      track.clase = 'fa-solid';
      this.spotifyService.guardarFavoritos(track);
    }
  }
}
