import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoSpotifyComponent } from './logo-spotify/logo-spotify.component';
import { ButtonComponent } from './button/button.component';



@NgModule({
  declarations: [
    LogoSpotifyComponent,
    ButtonComponent
  ],
  imports: [
    CommonModule
  ]
})
export class AtomsModule { }
