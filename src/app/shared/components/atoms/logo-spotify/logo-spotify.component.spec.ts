import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoSpotifyComponent } from './logo-spotify.component';

describe('LogoSpotifyComponent', () => {
  let component: LogoSpotifyComponent;
  let fixture: ComponentFixture<LogoSpotifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogoSpotifyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LogoSpotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
