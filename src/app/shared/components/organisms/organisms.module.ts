import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FeatureRoutingModule } from 'src/app/feature/feature-routing.module';
import { CardTrackComponent } from './card-track/card-track.component';
import { LoadingComponent } from './loading/loading.component';

@NgModule({
  declarations: [HeaderComponent, CardTrackComponent, LoadingComponent],
  imports: [CommonModule, FeatureRoutingModule],
  exports: [HeaderComponent, CardTrackComponent, LoadingComponent],
})
export class OrganismsModule {}
