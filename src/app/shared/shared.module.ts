import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoSpotifyComponent } from './components/atoms/logo-spotify/logo-spotify.component';
import { OrganismsModule } from './components/organisms/organisms.module';
import { AppRoutingModule } from '../app-routing.module';



@NgModule({
  declarations: [
    
  ],
  imports: [
    CommonModule,
    OrganismsModule,
    
  ], 
  exports:[
    OrganismsModule
  ]
})
export class SharedModule { }
