export interface Artist {
  external_urls: any;
  href: string;
  id: string;
  name: string;
  type: string;
  uri: string;
}
